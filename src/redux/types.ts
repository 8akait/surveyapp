export interface ActionType {
  type: string;
  payload: any;
}

// survey state
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';
export const LOADING = 'LOADING';
export const SET_EXPAND_CARD_ID = 'SET_EXPAND_CARD_ID';
export const SET_ANSWER = 'SET_ANSWER';
export const SET_SECONDARY_ANSWER = 'SET_SECONDARY_ANSWER';
export const SUBMIT_SUCCESS = 'SUBMIT_SUCCESS';

import {createStore, applyMiddleware, compose} from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import logger from 'redux-logger';

const store = createStore(
  reducers,
  {},
  compose(applyMiddleware(ReduxThunk, logger)),
);

export {store};

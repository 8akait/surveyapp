import {
  SUCCESS,
  ERROR,
  LOADING,
  ActionType,
  SET_EXPAND_CARD_ID,
  SET_ANSWER,
  SET_SECONDARY_ANSWER,
  SUBMIT_SUCCESS,
} from '../types';
import _ from 'lodash';

import {ToastAndroid} from 'react-native';

export type SecondaryType = 'radio_list' | 'checkl_list' | null;

export interface SurveyType {
  question: string | undefined;
  primary: any;
  secondary: string[] | undefined;
  secondary_type: SecondaryType;
}

export interface SurveyStateType {
  error: any;
  status: 'success' | 'error' | 'loading' | undefined;
  expandedIndex: number | undefined;
  questions: SurveyType[];
  userAnswers: any;
  userSecondaryAnswers: any;
}

const INITIAL_STATE: SurveyStateType = {
  error: undefined,
  status: undefined,
  expandedIndex: undefined,
  questions: [],
  userAnswers: undefined,
  userSecondaryAnswers: {},
};
const UserStateReducer = (
  state: SurveyStateType = INITIAL_STATE,
  action: ActionType,
): SurveyStateType => {
  switch (action.type) {
    case LOADING: {
      return {...state, status: 'loading'};
    }
    case SUCCESS: {
      ToastAndroid.show('Data fetched successfully', ToastAndroid.SHORT);
      const questions = action.payload;
      return {
        ...INITIAL_STATE,
        status: 'success',
        questions,
      };
    }
    case ERROR: {
      ToastAndroid.show('Error: ' + action.payload.error, ToastAndroid.SHORT);
      return {
        ...INITIAL_STATE,
        status: 'error',
        error: action.payload.error,
      };
    }
    case SET_EXPAND_CARD_ID: {
      return {
        ...state,
        expandedIndex: action.payload,
      };
    }
    case SET_ANSWER: {
      const {question, answer} = action.payload;
      const ans = {
        [question]: answer,
      };
      const userAns = {...state.userAnswers, ...ans};
      return {
        ...state,
        userAnswers: userAns,
      };
    }
    case SET_SECONDARY_ANSWER: {
      const {question, answer} = action.payload;
      const {userSecondaryAnswers} = state;
      const oldAns = userSecondaryAnswers[question]
        ? userSecondaryAnswers[question]
        : [];
      const ans = {
        [question]: _.uniq([...oldAns, answer]),
      };
      const userAns = {...state.userSecondaryAnswers, ...ans};
      return {
        ...state,
        userSecondaryAnswers: userAns,
      };
    }
    case SUBMIT_SUCCESS: {
      ToastAndroid.show('Data successfully send', ToastAndroid.SHORT);
      return {
        ...INITIAL_STATE,
        questions: state.questions,
        status: 'success',
      };
    }
    default: {
      return state;
    }
  }
};

export default UserStateReducer;

import {
  SUCCESS,
  ERROR,
  LOADING,
  SET_EXPAND_CARD_ID,
  SET_ANSWER,
  SET_SECONDARY_ANSWER,
  SUBMIT_SUCCESS,
} from '../types';

import axios from 'axios';
import {Dispatch} from 'redux';

const questionSuccess = (dispatch: Dispatch, user: any) => {
  dispatch({
    type: SUCCESS,
    payload: user,
  });
};
const questionFailed = (dispatch: Dispatch, error: any) => {
  dispatch({
    type: ERROR,
    payload: error,
  });
};
const questionLoading = (dispatch: Dispatch) => {
  dispatch({
    type: LOADING,
    payload: undefined,
  });
};

export const getQuestions = () => {
  return async (dispatch: Dispatch) => {
    questionLoading(dispatch);
    try {
      const response = await axios.get(
        'https://demo2000438.mockable.io/questions',
      );
      const {status, data} = response;
      const questions = data.data;
      if (status === 200) {
        return questionSuccess(dispatch, questions);
      } else {
        questionFailed(dispatch, {error: 'somthing went wrong'});
      }
    } catch (error) {
      questionFailed(dispatch, {error});
    }
  };
};

export const setExpandCardId = (index: number) => {
  return {
    type: SET_EXPAND_CARD_ID,
    payload: index,
  };
};

export const setAnswer = ({
  question,
  answer,
}: {
  question: number;
  answer: any;
}) => {
  return {
    type: SET_ANSWER,
    payload: {question, answer},
  };
};
export const setSecondaryAnswer = ({
  question,
  answer,
}: {
  question: number;
  answer: any;
}) => {
  return {
    type: SET_SECONDARY_ANSWER,
    payload: {question, answer},
  };
};

const submitSuccess = (dispatch: Dispatch) => {
  dispatch({
    type: SUBMIT_SUCCESS,
    payload: undefined,
  });
};
export const submitAnswer = (userAnswers: any) => {
  return async (dispatch: Dispatch) => {
    questionLoading(dispatch);
    try {
      const response = await axios.post(
        'https://demo2000438.mockable.io/feedback',
        {data: userAnswers},
      );
      const {status, data} = response;
      const questions = data.data;
      if (status === 200) {
        return submitSuccess(dispatch);
      } else {
        questionFailed(dispatch, {error: 'somthing went wrong'});
      }
    } catch (error) {
      questionFailed(dispatch, {error});
    }
  };
};

import {combineReducers} from 'redux';
import surveyReducer from './survey/survey.reducer';

export default combineReducers({
  surveyState: surveyReducer,
});

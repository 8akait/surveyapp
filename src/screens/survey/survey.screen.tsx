import React, {Component} from 'react';
import {Text, View, StyleSheet, ActivityIndicator, Button} from 'react-native';
import axios from 'axios';
import SurveyCard from './survey.card';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import * as SurveyActions from '../../redux/survey/survey.action';
import {SurveyStateType, SurveyType} from '../../redux/survey/survey.reducer';
import SurveyStatusScreen from './survey.status.screen';

export type SecondaryType = 'radio_list' | 'checkl_list' | null;

export interface Props {
  surveyState: SurveyStateType;
  surveyActions: any;
}
interface State {}

class SurverScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.onClickCard = this.onClickCard.bind(this);
  }
  async componentDidMount() {
    this.props.surveyActions.getQuestions();
  }
  onClickCard(index: number | string) {
    this.setState({
      expandedId: index,
    });
  }
  render() {
    const {container} = styles;
    const {surveyState} = this.props;
    const {status, questions, expandedIndex, error, userAnswers} = surveyState;
    let renderComponent: any = undefined;
    switch (status) {
      case 'success': {
        const output: any = [];
        if (questions && questions.length > 0) {
          questions.map((data: SurveyType, index: number) => {
            output.push(
              <SurveyCard
                key={index}
                survey={data}
                id={index}
                isExpanded={index === expandedIndex ? true : false}
              />,
            );
          });
        }
        renderComponent = output;
        break;
      }
      case 'error': {
        renderComponent = <Text>{error ? error.toString() : undefined}</Text>;
        break;
      }
      default: {
        renderComponent = <ActivityIndicator size="large" color="#0000ff" />;
        break;
      }
    }
    return (
      <View style={container}>
        {questions && questions.length > 0 ? (
          <SurveyStatusScreen
            total={questions.length}
            done={userAnswers ? Object.keys(userAnswers).length : 0}
          />
        ) : (
          undefined
        )}

        {renderComponent}

        {questions && questions.length > 0 && status !== 'loading' ? (
          <View style={{margin: 20}}>
            <Button
              title="submit"
              color="#0e9ed8"
              onPress={() => {
                this.props.surveyActions.submitAnswer(userAnswers);
              }}
            />
          </View>
        ) : (
          undefined
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    // borderWidth: 3,
    // borderColor: 'black',
    flex: 1,
    width: undefined,
  },
});

const mapStateToProps = (state: any) => {
  return {
    surveyState: state.surveyState,
  };
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    surveyActions: bindActionCreators(SurveyActions, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SurverScreen);

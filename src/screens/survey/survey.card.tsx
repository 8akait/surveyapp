import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Animated,
  TouchableOpacity,
  CheckBox,
  Switch,
} from 'react-native';
import {connect} from 'react-redux';
import {SurveyType, SecondaryType} from '../../redux/survey/survey.reducer';
import * as SurveyActions from '../../redux/survey/survey.action';
import {bindActionCreators, Dispatch} from 'redux';

const ANIMATION_DURATION = 350;
const SECONDARY_LIST_HEIGHT = 60;
const BUTTON_HEIGHT = 50;
const CARD_INITIAL_HEIGHT = 60;

export interface Props {
  survey: SurveyType;
  isExpanded: boolean;
  id: string | number;
  surveyActions: any;
}
interface State {
  isDisplaySecondaryAnswer: boolean;
  checkbox: {[x: string]: boolean};
}

class SurveyCard extends Component<Props, State> {
  private animation: Animated.Value;
  constructor(props: Props) {
    super(props);

    this.animation = new Animated.Value(0);
    this.setAnswer = this.setAnswer.bind(this);
    this.state = {
      isDisplaySecondaryAnswer: false,
      checkbox: {},
    };
  }
  shouldComponentUpdate(nextProps: Props, nextState: State) {
    if (nextProps.isExpanded !== this.props.isExpanded) {
      return true;
    } else if (
      nextState.isDisplaySecondaryAnswer !== this.state.isDisplaySecondaryAnswer
    ) {
      return true;
    } else if (
      JSON.stringify(nextState.checkbox) !== JSON.stringify(this.state.checkbox)
    ) {
      return true;
    }
    return false;
  }
  static getDerivedStateFromProps(nextProps: Props, prevState: State) {
    if (!nextProps.isExpanded) {
      return {
        isDisplaySecondaryAnswer: false,
      };
    }
    return {};
  }
  startAnimation() {
    const {isExpanded} = this.props;
    const {isDisplaySecondaryAnswer} = this.state;
    return Animated.timing(this.animation, {
      toValue: isExpanded ? (isDisplaySecondaryAnswer ? 2 : 1) : 0,
      duration: ANIMATION_DURATION,
    });
  }
  getCardStyle(secondasryListCount: number) {
    const minHeight = this.animation.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [
        CARD_INITIAL_HEIGHT,
        CARD_INITIAL_HEIGHT + BUTTON_HEIGHT,
        SECONDARY_LIST_HEIGHT * secondasryListCount,
      ],
    });
    return {
      ...styles.card,
      height: undefined,
      minHeight: minHeight,
    };
  }

  setAnswer(answer: string) {
    const {surveyActions, survey} = this.props;
    if (answer === 'No') {
      return this.setState({isDisplaySecondaryAnswer: true}, () => {
        surveyActions.setAnswer({
          question: survey.question,
          answer,
        });
      });
    }
    return surveyActions.setAnswer({
      question: survey.question,
      answer,
    });
  }
  setSecondaryAnswer(answer: string, isSelected: boolean, isSwitch: boolean) {
    const {surveyActions, survey} = this.props;
    if (isSwitch) {
      this.setState({checkbox: {[answer]: isSelected}}, () => {
        return surveyActions.setSecondaryAnswer({
          question: survey.question,
          answer: isSelected ? answer : undefined,
        });
      });
    }
    return surveyActions.setSecondaryAnswer({
      question: survey.question,
      answer: isSelected ? answer : undefined,
    });
  }
  renderAnswer(answer: string[] | undefined) {
    const {primaryAnswerButton, primaryAnswerText} = styles;
    if (answer && answer.length > 0) {
      return answer.map((ans: any, index: number) => (
        <TouchableOpacity
          key={index}
          style={primaryAnswerButton}
          onPress={() => this.setAnswer(ans)}>
          <Text style={primaryAnswerText}>{ans}</Text>
        </TouchableOpacity>
      ));
    }
    return <></>;
  }

  renderSecondaryAnswer(answer: string[] | undefined, type: SecondaryType) {
    const {
      secondaryAnswerContainer,
      secondaryAnsText,
      secondaryAnswerList,
    } = styles;
    const output: any[] = [];
    if (answer && answer.length > 0) {
      answer.map((ans: string, index: number) =>
        output.push(
          <View style={secondaryAnswerList} key={index}>
            <Text style={secondaryAnsText}>{ans}</Text>
            {type && type === 'checkl_list' ? (
              <CheckBox
                onValueChange={val => {
                  this.setSecondaryAnswer(ans, val, false);
                }}
              />
            ) : (
              <Switch
                value={this.state.checkbox[ans]}
                onValueChange={val => {
                  this.setSecondaryAnswer(ans, val, true);
                }}
              />
            )}
          </View>,
        ),
      );
      return <View style={secondaryAnswerContainer}>{output}</View>;
    }
    return undefined;
  }
  render() {
    this.startAnimation().start();
    const {touch, questionText, primaryAnswerContainer} = styles;
    const {survey, id, isExpanded, surveyActions} = this.props;
    const {primary, question, secondary, secondary_type} = survey;
    const {isDisplaySecondaryAnswer} = this.state;
    const secondasryListCount = secondary ? secondary.length : 0;
    return (
      <Animated.View style={this.getCardStyle(secondasryListCount)}>
        <TouchableOpacity
          style={touch}
          onPress={() => surveyActions.setExpandCardId(id)}>
          <Text style={questionText}>{question}</Text>
          {isExpanded && !isDisplaySecondaryAnswer ? (
            <View style={primaryAnswerContainer}>
              {this.renderAnswer(primary)}
            </View>
          ) : (
            undefined
          )}
          {isExpanded && isDisplaySecondaryAnswer
            ? this.renderSecondaryAnswer(secondary, secondary_type)
            : undefined}
        </TouchableOpacity>
      </Animated.View>
    );
  }
}
const styles = StyleSheet.create({
  card: {
    // flex: 1,
    width: undefined,
    backgroundColor: '#0e9ed8',
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: {width: 3, height: 5},
    shadowOpacity: 0.7,
    shadowRadius: 4,
    elevation: 14,
    margin: 8,
    position: 'relative',
    overflow: 'hidden',
    borderRadius: 10,
  },
  touch: {
    // borderWidth: 2,
    // borderColor: 'red',
    flex: 1,
    width: undefined,
    justifyContent: 'space-around',
    position: 'relative',
    alignItems: 'center',
  },
  questionText: {
    flex: 0,
    fontSize: 21,
    fontWeight: '600',
    alignSelf: 'center',
    color: 'white',
    margin: 8,
  },
  primaryAnswerContainer: {
    flex: 2,
    // borderWidth: 1,
    // borderColor: 'green',
    flexDirection: 'row',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  primaryAnswerButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#096e97',
    margin: 10,
    borderRadius: 10,
    height: BUTTON_HEIGHT,
  },
  primaryAnswerText: {
    fontSize: 19,
    fontWeight: '500',
    color: 'white',
  },
  secondaryAnswerContainer: {
    flex: 1,
    width: undefined,
    flexDirection: 'column',
  },
  secondaryAnswerList: {
    // borderWidth: 2,
    // borderColor: 'yellow',
    padding: 10,
    margin: 5,
    flex: 1,
    flexDirection: 'row',
    width: undefined,
    height: SECONDARY_LIST_HEIGHT,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#074f6c',
    borderRadius: 5,
  },
  secondaryAnsText: {
    fontSize: 14,
    fontWeight: '500',
    color: '#ddd',
  },
});
const mapStateToProps = (state: any) => {
  return {
    surveyState: state.surveyState,
  };
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    surveyActions: bindActionCreators(SurveyActions, dispatch),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SurveyCard);

import React from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';

const {width: screenWidth, height: screenHeight} = Dimensions.get('window');

export interface PropsType {
  total: number;
  done: number;
}

const surveyStatusScreen = ({total, done}: PropsType) => {
  const {container, textSTyle} = styles;
  return (
    <View style={container}>
      <Text style={textSTyle}>
        Status: {done}/{total}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    maxHeight: 50,
    flex: 1,
    width: undefined,
    backgroundColor: '#333',
    justifyContent: 'center',
  },
  textSTyle: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 21,
    fontWeight: '500',
  },
});
export default React.memo(surveyStatusScreen);

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {Provider} from 'react-redux';
import {store} from './redux/store';
import SurveyScreen from './screens/survey/survey.screen';
import {Dimensions, Platform, PixelRatio} from 'react-native';
const {width: screenWidth, height: screenHeight} = Dimensions.get('window');

const App = () => {
  const {container} = styles;
  return (
    <>
      <Provider store={store}>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={container}>
            <SurveyScreen />
          </View>
        </ScrollView>
      </Provider>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    // borderWidth: 3,
    // borderColor: 'purple',
    flex: 1,
    width: undefined,
    height: screenHeight,
  },
  scrollView: {
    backgroundColor: 'white',
  },
});

export default App;
